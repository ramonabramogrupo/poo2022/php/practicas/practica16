<?php

/**
 * Description of Dimension
 *
 * @author Profesor Ramon
 */
class Dimension {
    public float $alto=0;
    public float $ancho=0;
    public float $profundidad=0;
    
    public function __construct(float $alto=0, float $ancho=0, float $profundidad=0) {
        $this->alto = $alto;
        $this->ancho = $ancho;
        $this->profundidad = $profundidad;
    }
    
    public function getAlto(): float {
        return $this->alto;
    }

    public function getAncho(): float {
        return $this->ancho;
    }

    public function getProfundidad(): float {
        return $this->profundidad;
    }

    public function setAlto(float $alto): void {
        $this->alto = $alto;
    }

    public function setAncho(float $ancho): void {
        $this->ancho = $ancho;
    }

    public function setProfundidad(float $profundidad): void {
        $this->profundidad = $profundidad;
    }
    
    public function getVolumen() {
        return $this->alto*$this->ancho*$this->profundidad;
    }
    
    public function __toString() {
        $salida="Ancho= {$this->ancho}";
        $salida.="<br>Alto= {$this->alto}";
        $salida.="<br>Profundidad= {$this->profundidad}";
        $salida.="<br>Volumen Maximo= {$this->getVolumen()}";
        return $salida;
    }



}
