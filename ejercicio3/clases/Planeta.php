<?php

/**
 * Description of Planeta
 *
 * @author Profesor Ramon
 */

class Planeta {
    public ?string $nombre=NULL ;
    public int $cantidadSatelites=0;
    public float $masa=0;
    public float $volumen=0;
    public int $diametro=0;
    public int $distancia=0;
    public string $tipo;
    public bool $observable=false;
    const UA=149597870;
    
    public function __construct(?string $nombre=NULL, int $cantidadSatelites=0, float $masa=0, float $volumen=0, int $diametro=0, int $distancia=0, string $tipo="ENANO", bool $observable=false) {
        $this->nombre = $nombre;
        $this->cantidadSatelites = $cantidadSatelites;
        $this->masa = $masa;
        $this->volumen = $volumen;
        $this->diametro = $diametro;
        $this->distancia = $distancia;
        $this->tipo = $tipo;
        $this->observable = $observable;
    }
    
    public function densidad():float{
        return $this->masa/$this->volumen;
    }
    
    public function exterior(): bool{
        if($this->distancia>=3.4*self::UA){
            return true;
        }else{
            return false;
        }
    }

}
