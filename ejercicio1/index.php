<?php

// autocarga de clases
spl_autoload_register(function ($clase) {
    require 'clases/' . $clase . '.php';
});

$persona1=new Persona("Pedro", "Perez", "10510523", 1998);
$persona2=new Persona("Luis","Leon","1000002",2001);        

var_dump($persona1);
var_dump($persona2);

$persona1->imprimir();

echo $persona1->imprimir1();

echo $persona2->imprimir1();