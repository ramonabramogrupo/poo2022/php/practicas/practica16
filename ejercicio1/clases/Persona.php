<?php

/**
 * Description of Persona
 *
 * @author Profesor Ramon
 */
class Persona {
    public string $nombre;
    public string $apellidos;
    public string $numeroDocumentoIdentidad;
    public int $annoNacimiento;
    
    public function __construct(string $nombre, string $apellidos, string $numeroDocumentoIdentidad, int $annoNacimiento) {
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->numeroDocumentoIdentidad = $numeroDocumentoIdentidad;
        $this->annoNacimiento = $annoNacimiento;
    }
    
    /**
     * @deprecated sustituido por el nuevo imprimir
     */
    
    public function imprimir() : void{
        echo "Nombre={$this->nombre}";
        echo "<br>";
        echo "Apellidos={$this->apellidos}";
        echo "<br>";
        echo "Número de documento de identidad={$this->numeroDocumentoIdentidad}";
        echo "<br>";
        echo "Año de nacimiento={$this->annoNacimiento}";
        echo "<br>";
    }
    
    public function imprimir1() : string{
        $salida="Nombre={$this->nombre}";
        $salida = $salida . "<br>";
        $salida = $salida . "Apellidos={$this->apellidos}";
        $salida = $salida . "<br>";
        $salida = $salida . "Número de documento de identidad={$this->numeroDocumentoIdentidad}";
        $salida = $salida . "<br>";
        $salida = $salida . "Año de nacimiento={$this->annoNacimiento}";
        $salida = $salida . "<br>";
        return $salida;
    }
    
    
}
