<?php

// autocarga de clases
spl_autoload_register(function ($clase) {
    require 'clases/' . $clase . '.php';
});

$persona1=new Persona("Pedro", "Perez", "10510523", 1998,"Argentina","H");
$persona2=new Persona("Luis","Leon","1000002",2001,"España","H");        

var_dump($persona1);
var_dump($persona2);


echo $persona1->imprimir();

echo $persona2->imprimir();