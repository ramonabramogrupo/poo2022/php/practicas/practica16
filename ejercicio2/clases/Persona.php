<?php

/**
 * Description of Persona
 *
 * @author Profesor Ramon
 */
class Persona {
    public string $nombre;
    public string $apellidos;
    public string $numeroDocumentoIdentidad;
    public int $annoNacimiento;
    public string $paisNacimiento;
    public string $genero;
    
    public function __construct(string $nombre, string $apellidos, string $numeroDocumentoIdentidad, int $annoNacimiento, string $paisNacimiento, string $genero) {
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->numeroDocumentoIdentidad = $numeroDocumentoIdentidad;
        $this->annoNacimiento = $annoNacimiento;
        $this->paisNacimiento = $paisNacimiento;
        $this->genero = $genero;
    }

   
    public function imprimir() : string{
        $salida="Nombre={$this->nombre}";
        $salida = $salida . "<br>";
        $salida = $salida . "Apellidos={$this->apellidos}";
        $salida = $salida . "<br>";
        $salida = $salida . "Número de documento de identidad={$this->numeroDocumentoIdentidad}";
        $salida = $salida . "<br>";
        $salida = $salida . "Año de nacimiento={$this->annoNacimiento}";
        $salida = $salida . "<br>";
        $salida = $salida . "Pais de Nacimiento={$this->paisNacimiento}";
        $salida = $salida . "<br>";
        $salida = $salida . "Genero={$this->genero}";
        $salida = $salida . "<br>";
        return $salida;
    }
    
    
}
