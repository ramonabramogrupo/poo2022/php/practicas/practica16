<?php

/**
 * Description of Superheroe
 *
 * @author Profesor Ramon
 */
class Superheroe {
   public string $nombre;
   public string $descripcion="";
   public bool $capa=false;
   
   
   public function __construct(string $nombre) {
       $this->nombre = $nombre;
   }

   public function getNombre(): string {
       return $this->nombre;
   }

   public function getDescripcion(): string {
       return $this->descripcion;
   }

   public function getCapa(): string {
       if($this->capa){
           return "Tiene Capa";
       }else{
           return "No tiene capa";
       }
   }

   public function setNombre(string $nombre): void {
       $this->nombre = $nombre;
   }

   public function setDescripcion(string $descripcion): void {
       $this->descripcion = $descripcion;
   }

   public function setCapa(bool $capa): void {
       $this->capa = $capa;
   }
   
   public function __toString() {
       $resultado=$this->nombre . "<br>";
       $resultado.=$this->descripcion . "<br>";
       $resultado.=$this->getCapa();
       return $resultado;
   }


   
}
